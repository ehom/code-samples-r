#!/usr/local/bin/node

'use strict;'

/*
Author: 

    ehom

Description:

    This function takes a string of brackets and 
    returns the minimum number of brackets
    you'd have to add to the string to make it correctly match.
*/

function bracket_match(bracket_string) {
  var left_parens = 0;
  var right_parens = 0;

  for (ch of bracket_string) {
    if (ch == '(') {
      left_parens++;
    } else if (ch == ')') {
      if (left_parens) {
        left_parens--;
      } else {
      	right_parens++;
      }
    }
  }
  
  return left_parens + right_parens;;
}

var test_cases = [
 [ "(()())", 0 ],
 [ "((())", 1 ],
 [ "())", 1 ],
 [ "(((", 3 ],
 [ ")))", 3 ],
 [ ")(", 2 ]
];

for (str of test_cases) {
  console.log(str[0], "returned:" , bracket_match(str[0]), "expected:", str[1]);
}
