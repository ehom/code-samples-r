#!/usr/local/bin/node

'use strict;'

/*
Author: 

    ehom

Description: 

    Write a function that takes a string and returns its palindromic score.

*/

/* 
This version uses the reverse() function from Array.
*/

function almost_palindromes2(str) {
  var reversed_str = str.split("").reverse().join("");
  var length = str.length;
  var score = 0;
  for (var i = 0; i < length; i++) {
    if (str[i] !== reversed_str[i]) {
      score++;
    }
  }
  return score;
}

console.log("Created a copy of the string and reversed it then\ncompare them, character by character...");
console.log(almost_palindromes2("abba"));
console.log(almost_palindromes2("abcdcaa"));
console.log(almost_palindromes2("aaabbb"));

/* 
This version doesn't make a copy of the string.
Instead, it uses the index variable to reference the end 
of the string by substracting it from variable end_index.
*/

function almost_palindromes(str) {
  var score = 0;
  var end_index = str.length - 1;
  for (var index = 0; index <= end_index; index++) {
    if (str[index] != str[end_index - index]) {
      score++;
    }
  }
  return score;
}

console.log("Using the efficient version...");
console.log(almost_palindromes("abba"));
console.log(almost_palindromes("abcdcaa"));
console.log(almost_palindromes("aaabbb"));

