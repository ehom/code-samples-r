//
// fizzbuzz.js
//

$(document).ready(function(){

  var view = {
    render: function(list) {
      var html_text = '';
      // 1. Iterate the list, format each entry with html. Accumulate the html text
      // 2. Push the html text to the DOM all @ once

      list.forEach(function(entry, index, array) {
        if (isNaN(entry)) {
          html_text += "<p><span class=\"fizzbuzz\">" + entry + "</span></p>";
        } else {
          html_text += "<p class=\"numberDisplay\">" + entry + "</p>";
        }
      });
      $("body").append(html_text);
    }
  };

  // 1. Indicate which numbers are divisible by 3 and
  //    which ones are divisible by 5 and
  //    which ones are divisible by both 3 and 5.
  //
  // 2. Pass that list to the view to render.

  var controller = {
    init: function() {
      var list = [];
      var output = '';

      for (var i = 1; i <= 100; i++) {
        output = '';

        if (i % 3 == 0) {
          output = "Fizz";
        }

        if (i % 5 == 0) {
          output += "Buzz";
        }

        if (output === '') {
          output = i.toString();
        }

        list.push(output)
      } // end-for

      view.render(list);
    }
  };

  controller.init();
});
