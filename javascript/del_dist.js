#!/usr/local/bin/node

'use strict';

/*
Author: ehom

Description: Find the deletion distrance betwen two strings.

*/

function deletion_distance(str1, str2) {

    var left = str1; 
    var right = str2; 

    if (str1.length < str2.length) {
      left = str2;
      right = str1;
    }
    
    var distance = left.length - right.length;
    left = left.slice(distance);

/*  
    console.log("left:", left);
    console.log("right:", right);
*/
    var length = right.length;
    for (var index = 0; index < length; index++) {
    	if (left.slice(index) === right.slice(index)) {
        	break;
        }
        distance++;
    }
    return distance;
}

var test_cases = [
    [ "at", "cat", 1 ],
    [ "boat", "got", 3 ],
    [ "thought", "sloughs", 6 ],

];

for (var str of test_cases) {
    console.log("\"%s\" \"%s\"",  str[0], str[1]);
    console.log("\tReturns:", deletion_distance(str[0], str[1]), "Expected:", str[2]);
}

