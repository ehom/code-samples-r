#!/usr/bin/python

""" 
This code challenge came from udacity's cs101 course

https://classroom.udacity.com/courses/cs101/lessons/48689154/concepts/6986185930923 

"""


""" 
How to determine whether a year is a leap year

Reference:
	https://support.microsoft.com/en-us/kb/214019

To determine whether a year is a leap year, follow these steps:

1. If the year is evenly divisible by 4, go to step 2. Otherwise, go to step 5.

2. If the year is evenly divisible by 100, go to step 3. Otherwise, go to step 4.

3. If the year is evenly divisible by 400, go to step 4. Otherwise, go to step 5.

4. The year is a leap year (it has 366 days).

5. The year is not a leap year (it has 365 days). 

"""


""" 
This function is the implementation of how to check if a year is a leap year

"""

def isLeapYear(year):
    if year % 4 == 0 and year % 100 != 0:
        return True

    if year % 400 == 0:
        return True

    return False


"""
Here's another way to implement it
NOTE: isLeapYear2() is not used

Reference:
    en.wikipedia.org/wiki/Leap_year

"""

def isLeapYear2(year):
	if year % 400 == 0:
		return True

	if year % 100 == 0:
		return False

	if year % 4 == 0:
		return True

	return False


""" 
This function returns the number of days given the year and month

Reference:
    https://en.wikipedia.org/wiki/Month#Julian_and_Gregorian_calendars

"""

def daysInMonth(year, month):
    days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    days_in_month = days[month - 1]

    if isLeapYear(year) and month == 2:
        days_in_month = 29

    return days_in_month    


"""
Function Name:

    nextDay()

Returns: 

    next day in the calendar

"""

def nextDay(year, month, day):
    if day < daysInMonth(year, month):
        return year, month, day + 1
    else:
        if month == 12:
            return year + 1, 1, 1
        else:
            return year, month + 1, 1
        
"""
Function Name:

    dayIsBefore()

Description:

    This function checks to make sure the first date is before the second date

Returns:
    True if year1-month1-day1 is before year2-month2-day2. 
    Otherwise, returns False.

"""

def dateIsBefore(year1, month1, day1, year2, month2, day2):
    if year1 < year2:
        return True

    if year1 == year2:
        if month1 < month2:
            return True
        if month1 == month2:
            return day1 < day2

    return False        


"""
This function returns the days between the (two) dates.

Returns:
    The number of days between year1/month1/day1 and year2/month2/day2. 
    Assumes inputs are valid dates in Gregorian calendar.

"""

def daysBetweenDates(year1, month1, day1, year2, month2, day2):
    # program defensively! Add an assertion if the input is not valid!
    assert not dateIsBefore(year2, month2, day2, year1, month1, day1)

    days = 0
    
    while dateIsBefore(year1, month1, day1, year2, month2, day2):
        year1, month1, day1 = nextDay(year1, month1, day1)
        days += 1

    return days


"""
Here are the test cases to make sure daysBetweenDays() is working properly.

"""

def test():
    test_cases = [((2012,1,1,2012,2,28), 58), 
                  ((2012,1,1,2012,3,1), 60),
                  ((2011,6,30,2012,6,30), 366),
                  ((2011,1,1,2012,8,8), 585 ),
                  ((1900,1,1,1999,12,31), 36523)]
    
    for (args, answer) in test_cases:
        result = daysBetweenDates(*args)
        if result != answer:
            print ("Test with data:", args, "failed")
        else:
            print ("Test case passed!")


# Run the test case here

test()


