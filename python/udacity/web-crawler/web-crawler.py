#!/usr/bin/python

"""
function:
    get_page 

returns:
    html content

"""

def get_page(url):
    try:
        import urllib
        return urllib.urlopen(url).read()
    except:
        return ""

"""
function:
    get_next_target

returns:
    a URL and end_quote which is a pos in the html page

"""

def get_next_target(page):
    start_link = page.find('<a href=')
    if start_link == -1: 
        return None, 0
    start_quote = page.find('"', start_link)
    end_quote = page.find('"', start_quote + 1)
    url = page[start_quote + 1:end_quote]
    return url, end_quote

# combine two lists

def union(p,q):
    for e in q:
        if e not in p:
            p.append(e)

# ----------

def get_all_links(page):
    links = []
    while True:
        url, endpos = get_next_target(page)
        if url:
            links.append(url)
            page = page[endpos:]
        else:
            break
    return links

#
# function:
#     add_page_to_index
#
# Inputs:
#   - index
#   - url (String)
#   - content (String)
#
# It should update the index to include all of the word occurences 
# found in the page content by adding the url to the word's associated url list.

def add_to_index(index, keyword, url):
    # format of index: [[keyword, [[url, count], [url, count],..]],...]
    for entry in index:
        if entry[0] == keyword:
            for urls in entry[1]:
                if urls[0] == url:
                    return
            entry[1].append([url,0])
            return
    # not found, add new keyword to index
    index.append([keyword, [[url,0]]])

# ---------- 

def clean_page(htmlPage):
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(htmlPage, 'html.parser')
    for script in soup(["script", "style"]):
        script.extract()
    # get text
    text = soup.get_text()
    
    # break into lines and remove leading and trailing space on each line
    lines = (line.strip() for line in text.splitlines())

    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split(" "))

    # drop blank lines
    text = '\n'.join(chunk for chunk in chunks if chunk)
    return text


def add_page_to_index(index,url,htmlContent):
    cleanContent = clean_page(htmlContent)
    words = cleanContent.split();

    for word in words:
        add_to_index(index, word, url)

#
# One way search engines rank pages is to count the number of times a
# searcher clicks on a returned link.
# This indicates that the person doing the query thought this was a useful
# link for the query, so it should be higher in the rankings next time.
#
# The index is modified such that for each url in a list for a keyword, 
# there is also a number that counts the number of times a user
# clicks on that link for this keyword.
#
# The result of lookup(index,keyword) should be a list of url entries, 
# where each url entry is a list of a url and a number
# indicating the number of times that url was clicked for the query keyword.
#
# Function record_user_click(index,word,url) modifies the entry in the index for
# the input word by increasing the count associated with the url by 1.
#

def record_user_click(index, keyword, url):
    urls = lookup(index, keyword)
    if urls:
        for entry in urls:
            if entry[0] == url:
                entry[1] = entry[1]+1

# ------------

def print_index(index):
    for entry in index:
        print entry[0]
        urls = entry[1]
        for url in urls:
            print ' ' * 4, url

# ------------

def print_pages_crawled(list):
    print "Pages crawled:"
    index = 1
    for url in list:
        print '%5s' % index, url
        index = index + 1

# ------------
# crawl_web() is the main function

def craw_web(seedPage, maxPages=5):
    pagesToCrawl = [seedPage]
    pagesCrawled = []
    links = []
    index = []
    while pagesToCrawl and len(pagesCrawled) < maxPages:
        pageURL = pagesToCrawl.pop();
        if pageURL not in pagesCrawled:
            htmlContent = get_page(pageURL)
            add_page_to_index(index, pageURL, htmlContent)

            # update pages to crawl
            links = get_all_links(htmlContent)
            # used union to avoid duplicate links
            union(pagesToCrawl, links)

            if pageURL.startswith('http'):
                # update pages crawled
                pagesCrawled.append(pageURL)

                # debug message
                print len(pagesCrawled)

    # @ the moment, just return the list of pages crawled
    # return pagesCrawled
    print_pages_crawled(pagesCrawled)

    return index

# ------------

# pagesCrawled = craw_web("http://udacity.com/cs101x/index.html")
# pagesCrawled = craw_web("http://dmoz.org")
index = craw_web("http://www.yahoo.com")
print_index(index)

