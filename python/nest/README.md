extract-ip-addrs.py
===================

This Python 3 script extracts IP addresses from a log file ("debug.log" by default) and prints them to the console.

----------


How to Run this script
-------------
> $ extract-ip-addrs.py

Read "debug.log" and print IP addresses out.

> $ extract-ip-addrs.py debug

Read "debug.log" and print IP addresses out. Show **BAD** IP addresses as well.

> $ extract-ip-addrs.py   ../application.log

Read "application.log" and print IP addresses out. 

>  $ extract-ip-addrs.py ./application.log debug

Read "application.log" and print IP addresses out. Show **BAD** IP addresses as well.

Error Messages
------------

- File Not Found

- Usage


