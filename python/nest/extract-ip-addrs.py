#!/usr/local/bin/python3

#
# Filename: 
#
# extract-ip-addrs.py
#
# Author: 
#
# Erwin Hom
#
# Description:
#
# * Write a function that takes a filename for a file on the local machine that contains general debug logs and IP address information.
# * Parse the file for IP addresses, eliminate duplicates, and filter invalid strings that look kind of like an IP address (678.345.1.3).
# * Print the list of IPs.
#
# Keep research to just core python: https://docs.python.org
#

import os, sys, ipaddress

SCRIPT_FNAME = os.path.basename(sys.argv[0]);
DEFAULT_LOG_FNAME = "debug.log"

def read_log_file(filename):
	try:
		with open(filename,"r")	as fh:
			lines = fh.readlines()
	except FileNotFoundError as err:
		print("{}: {}".format(SCRIPT_FNAME, err))
		sys.exit(1)
	except:
		print("{}: Can't read log file \"{}\".({})".format(SCRIPT_FNAME, filename, sys.exc_info()[0]))
		sys.exit(1)
	else:
		fh.close()
	return lines

def print_IP_addresses(filename, debugging=False):

	lines = read_log_file(filename) 

	# use a dictionary to contain the set of unique IP address values
	addresses = dict()

	for line in lines:
		# the IP address is the second field
		# remove leading and trailing spaces 
		# to make sure the address string value is "clean" before validation

		strIPaddr = line.split('::')[1].strip()
		try:
			# ipaddress.IPv4Address will raise an exception if address value is invalid
			int_addr = ipaddress.IPv4Address(strIPaddr).packed
			
			# store ip addr string as value
			addresses[int_addr] = strIPaddr
		except:
			# don't put bad address values in the address book (dictionary)
			if debugging:
				print("*** BAD address: {0}".format(strIPaddr))
			pass

	for key in addresses:
		print(addresses[key])

def main():
	(log_filename, debugging) = (DEFAULT_LOG_FNAME, False)

	if len(sys.argv) > 1: 
		for arg in sys.argv[1:]:
			if arg.lower() == 'debug':
				debugging = True
			elif arg.endswith(".log"):
				log_filename = arg
			else:	
				print("Usage: {} [debug][filename.log]".format(SCRIPT_FNAME));
				sys.exit(1)
	print_IP_addresses(log_filename, debugging)
	
if __name__ == "__main__": main()

### end-of-script ###
