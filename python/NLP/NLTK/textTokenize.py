#!/usr/local/bin/python3

import sys
import nltk
import nltk.data

print("~~~")
print("Using Python version %s ..." % sys.version)
print("~~~")
print("Using NLTK version %s ..." % nltk.__version__)
print("~~~")

# Chapter 1. Tokenizing Text and WordNet 

para = "Hello World. It's good to see you. Thanks for buying this book."

# Break the paragraph into sentences

print("Breaking (tokenizing) the paragraph into sentences...")

from nltk.tokenize import sent_tokenize
sent_tokens = sent_tokenize(para)

for sent_token in sent_tokens:
    print('"' + sent_token + '"')

# Note: when calling sent_tokenize(), the pickle file is loaded on demand.
# You can create an instance of the tokenizer so that the app doesn't have to 
# keep loading it (the pickle file)

# By the way, you can also tokenize sentences in other languages

# Tokenizing sentences into words
from nltk.tokenize import word_tokenize

print("\nTokenizing sentences into words...")

for sent_token in sent_tokens:
    print(word_tokenize(sent_token))

