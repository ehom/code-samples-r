#!/usr/local/bin/python3

from nltk.corpus import wordnet

synset = wordnet.synsets('cookbook')[0]

print(synset.name())
print(synset.definition())
print(synset)

print("\nMethods that can be called from a Synset:")
print(dir(synset))

print("\n", wordnet.synsets('cooking')[0].examples())

print(synset.hypernyms())

print("\nHyponyms:")
hyponyms = synset.hypernyms()[0].hyponyms()
for hyponym in hyponyms:
    print(' ' * 4, hyponym)

print("\nHypernym Paths:")
for path in (synset).hypernym_paths()[0]:
    print(' ' * 4, path)
