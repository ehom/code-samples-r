#!/usr/bin/python

from nltk.corpus import genesis
from nltk import bigrams
from nltk import ConditionalFreqDist

#
# This python script analyzes the Book of Genesis
#

def generate_model(cfdist, word, num=15):
    for i in range(num):
        word = cfdist[word].max()
        print word

def print_generator_obj(obj, maxnum):
    """ 
    Call the next() method until 
    exception StopIteration is thrown

    """
    count = 0
    while count < maxnum:
        try:
            print obj.next();
            count = count + 1
        except StopIteration:
            break

#
#

print "Analyzing the Book of Genesis...."

text = genesis.words('english-kjv.txt')

print "The length of this text is", len(text), "words"

print "~~~ Sorted Tokens (first 30) ~~~"
print sorted(set(text))[:30]


"""
bigrams() returns a generator object

"""

list_of_bigrams = bigrams(text)

print "~~~ List of Bigrams (first 30) ~~~"
print_generator_obj(list_of_bigrams, 30)

# call bigrams() again since calling next on the generator object
# takes the bigrams out

list_of_bigrams = bigrams(text)
cfd = ConditionalFreqDist(list_of_bigrams)

print cfd['living']

generate_model(cfd, 'living')


