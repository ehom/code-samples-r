#!/usr/local/bin/python3

import nltk

print("\nUsing NLTK version {} ...\n".format(nltk.__version__))

sentence = """
At eight o'clock on Thursday morning
Arthur didn't feel very good.
"""

print("Here's a sentence:")
print(sentence)

print("\nTokenize and tag some text:")

tokens = nltk.word_tokenize(sentence)
print(tokens, '\n')
print("\n".join(tokens))


print("\nIdentify named entities:")
tagged = nltk.pos_tag(tokens)
print(tagged)

# -----

print("\nUsing sentence tokenize...")
tokens = nltk.sent_tokenize(sentence)
print(tokens, '\n')
print("\n".join(tokens))

print("\nIdentify named entities:")
tagged = nltk.pos_tag(tokens)
print(tagged)

print("\nDisplaying a parse tree...")
from nltk.corpus import treebank
t = treebank.parsed_sents('wsj_0001.mrg')[1]
t.draw()
