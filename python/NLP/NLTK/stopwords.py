#!/usr/local/bin/python3

import nltk

print("Using NLTK version %s ..." % nltk.__version__)

from nltk.corpus import stopwords
english_stops = set(stopwords.words('english'))
print("\nHere is a list of all English stopwords(%d):" % len(english_stops))
print(english_stops)
#
print("\nHere's a sentence tokenized:")
words = ["Can't", "is", "a", "contraction"]
print(words)

print("\nHere's what's left after stopwords removed:")
print([word for word in words if word not in english_stops])

print("\nNLTK has stopwords for the following languages:")
print(stopwords.fileids())

#
dutch_stops = stopwords.words('Dutch')
print("\nHere's the list of Dutch stopwords(%d):" % len(dutch_stops));
print(dutch_stops)
