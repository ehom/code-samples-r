//
// utils.cpp
//

#include <iostream>
#include <vector>


void
print_vector(const std::vector<int>& v) {
  int length = 0;

  for (int i = 0, length = v.size(); i < length; i++) {
    std::cout << v[i] << " ";
  }
  std::cout << "\n" << std::endl;
}

// Generate random numbers between 0-99
const std::vector<int>&
random_numbers(int count) {
  static std::vector<int> numbers;
  numbers.clear();

  int number = 0;

  srand(time(NULL));

  for (int i = 0; i < count; i++) {
    number = rand() % 100;
    numbers.push_back(number);
  }
  return numbers;
}
