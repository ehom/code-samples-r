/*
 * utils.h
 */

#ifndef UTILS_H
#define UTILS_H

extern const std::vector<int>& random_numbers(int count);
extern void print_vector(const std::vector<int>& v);

#endif

