//
//
// Merge Sort
//
// https://www.thinkful.com/projects/merge-sort-633/
//
// Milestone 1: Sort hardcoded lists
//
// Milestone 3: Generate random number lists

#include <iostream>
#include <vector>

#include "utils.h"

static const std::vector<int>& merge_sort(const std::vector<int>& list);
static const std::vector<int>& merge(std::vector<int>& left, std::vector<int>& right);
static void milestone1();
static void milestone3();

int 
main(int argc, char* argv[]) {

  milestone1();
  milestone3();

  return 0;
}

void milestone1() {
  // Construct a vector from a C array of integers
  // int list[] = { 23, 99, 42, 81, 89, 13, 64, 16 };
  // std::vector<int> input(list, list + sizeof(list)/sizeof(int));

  // you can do this in C++11
  // g++ test.cpp -o test -std=c++11
  std::vector<int> input = { 23, 99, 42, 81, 89, 13, 64, 16 };

  std::cout << "*** Unsorted ***" << std::endl;
  print_vector(input);
  std::cout << "*** Sorted ***" << std::endl;

  const std::vector<int>& result = merge_sort(input);
  print_vector(result);
}

void milestone3() {
  const std::vector<int>& input = random_numbers(25);

  std::cout << "*** Unsorted Random Numbers ***" << std::endl;
  print_vector(input);

  const std::vector<int>& result = merge_sort(input);

  std::cout << "*** Sorted ***" << std::endl;
  print_vector(result);
}

// Top-down implementation using lists

const std::vector<int>& 
merge_sort(const std::vector<int>& list) {

  std::vector<int> left_list;
  std::vector<int> right_list;

  if (list.size() <= 1) {
    return list;
  }

  int length = 0;

  for (int i = 0, length = list.size(); i < length; i++) {
    if (i % 2) {
      right_list.push_back(list[i]);
    } else {
      left_list.push_back(list[i]);
    }
  }

  // std::cout << "in merge_sort..." << std::endl;

  left_list = merge_sort(left_list);
  right_list = merge_sort(right_list);

  // std::cout << "*** left_list" << std::endl;
  // print_vector(left_list);
  ////  std::cout << "*** right_list" << std::endl;
  // print_vector(right_list);

  // std::cout << "before calling merge()" << std::endl;

  return merge(left_list, right_list);
}

const std::vector<int>& 
merge(std::vector<int>& left, std::vector<int>& right) {
  // can't return a local so tag it as static 
  // make sure it's cleared each time this function is entered.
  static std::vector<int> result;
  result.clear();

  while (!left.empty() && !right.empty()) {
    if (left.front() <= right.front()) {
      // std::cout << "move left_list into results" << std::endl;
      // print_vector(left);
      result.push_back(left.front());
      left.erase(left.begin());
      // std::cout << "moved left_list value into results" << std::endl;
      // print_vector(left);
    } else {
      result.push_back(right.front());
      right.erase(right.begin());
    }
  }

  while (!left.empty()) {
    result.push_back(left.front());
    left.erase(left.begin());
  }

  while (!right.empty()) {
    result.push_back(right.front());
    right.erase(right.begin());
  }
  // std::cout << "*** results *** " << std::endl;
  // print_vector(result);
  return result;
}

