//
// Milestone 1: Sort hardcoded lists
//
//   [ 5,1,4,2,8 ]
//
// Milestone 2: Measure performance
//
//   Print out the list each time there is a swap
//
// Milestone 3:
//
//   Generate a list of random numbers
//

// Note: 
//
//   This cpp file contains c++11 source code 
//
//   Make sure that c++11 is indicated on the build
//   command line.
// 
//   g++ app.cpp -o app -std=c++11
//

#include <iostream>
#include <vector>
#include "utils.h"

// instead of writing a function 'swap_int', 
// define a function template for swap_numbers().

template<typename T> 
void swap_numbers(T& v1, T& v2) {
  T temp = v1;
  v1 = v2;
  v2 = temp;
}

static void BubbleSort(std::vector<int>& list) {
  bool bSwapped = false;

  int nPass = 1;
  int length = list.size();

  do {
    bSwapped = false;
    std::cout << "Pass #" << nPass << ":" << std::endl;

    for (int i = 0; i < length; i++) {
      if (i+1 < length && list[i] > list[i+1]) {
        swap_numbers<int>(list[i], list[i+1]);
        bSwapped = true;
        std::cout.fill(' ');
        std::cout.width(3);
        std::cout << i+1 << ": ";
        print_vector(list);
      } // end-if
    } // end-for
    nPass++;

    if (!bSwapped) {
      std::cout << "... Nothing was swapped ..." << std::endl;
    }
  } while (bSwapped);
}

void milestone1() {
  std::cout << "*** MILESTONE 1" << std::endl;

  std::vector<int> numbers = { 5, 1, 4, 2, 8 };
  std::vector<int> unsorted(numbers);

  BubbleSort(numbers);

  std::cout << "Original List:" << std::endl;
  print_vector(unsorted);
  std::cout << "Sorted List:" << std::endl;
  print_vector(numbers);
}

void milestone3() {
  std::cout << "*** MILESTONE 3" << std::endl;

  std::vector<int> numbers = random_numbers(20);
  std::vector<int> unsorted(numbers);

  BubbleSort(numbers);

  std::cout << "-->List of Random Numbers:" << std::endl; 
  print_vector(unsorted);
  std::cout << "-->SORTED:" << std::endl; 
  print_vector(numbers);
}

int main() {
  std::cout << "Bubble Sort Demo" << std::endl;

  milestone1();
  milestone3();

  return 0;
}
