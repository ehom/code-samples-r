/*
 * Description:
 *
 *  This file contains an implementation of a singly-linked-list.
 *
 *  The inspiration for this project came from the following web page:
 *
 *    https://www.thinkful.com/projects/linked-list-634/
 */

#ifndef SINGLY_LINKED_LIST
#define SINGLY_LINKED_LIST

#include <iostream>
#include <iomanip>

using namespace std;

class Node {
  friend class LinkedList;

  public:
    Node(int value);

  private:
    int value;
    Node* next;
};

typedef Node* NodePtr;

class LinkedList {
  private:
    NodePtr head;
    NodePtr tail;
    int     nNodes;
  public:
    LinkedList();
    ~LinkedList();

  public:
    void AddFront(int value);
    void Display(void);
    void AddEnd(int value);
    void AddAtIndex(int value, int index);
    void RemoveFront(void);
    void RemoveEnd(void);
    void RemoveAtIndex(int index);
     int GetAtIndex(int index);
     int Count();
};

#endif 
