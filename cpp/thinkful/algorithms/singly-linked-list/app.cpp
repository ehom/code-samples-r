/*
 * Description:
 *
 *  This file contains an implementation of a singly-linked-list.
 *
 *  The inspiration for this project came from the following web page:
 *
 *    https://www.thinkful.com/projects/linked-list-634/
 */

#include <iostream>
#include <iomanip>

using namespace std;

#include "Debug.h"
#include "SinglyLinkedList.h"

int main(int argc, char* argv[]) {

  LinkedList list;

  list.AddFront(8);
  list.AddFront(10);   
  list.AddFront(42);   
  list.AddFront(100); 

  list.AddEnd(20000);
  list.AddEnd(30000);

  list.AddFront(1);

  list.Display();

  list.RemoveFront();
  list.Display();

  Debug::Log("The 3rd element is ", list.GetAtIndex(3));
  Debug::Log("The 100th element is ", list.GetAtIndex(100));
  
  list.AddAtIndex(888, 0);
  list.Display();

  Debug::Log("# of elements in list: ", list.Count()); 

  list.AddAtIndex(889, list.Count());
  list.Display();

  Debug::Log("# of elements in list: ", list.Count()); 

  list.AddAtIndex(777, list.Count() / 2); 
  list.Display();

  Debug::Log("# of elements in list: ", list.Count()); 

  return 0;
}
