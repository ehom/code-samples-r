//
// Debug.h
//

#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED

class Debug {
public:
    static void Log(const std::string &message, int value);
    static void Log(const std::string &message);
};

#endif
