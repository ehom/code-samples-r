/*
 * Description:
 *
 *  This file contains an implementation of a singly-linked-list.
 *
 *  The inspiration for this project came from the following web page:
 *
 *    https://www.thinkful.com/projects/linked-list-634/
 */

#include <iostream>
#include <iomanip>

using namespace std;

#include "Debug.h"
#include "SinglyLinkedList.h"

void Debug::Log(const std::string &message, int value) {
  cout << "DEBUG: " << message << value << endl;
}

void Debug::Log(const std::string &message) {
  cout << "DEBUG: " << message << endl;
}

Node::Node(int value) : next(NULL) {
  this->value = value;
}

// ctor
LinkedList::LinkedList() :
  head(NULL), tail(NULL), nNodes(0) { 
}

// dtor
LinkedList::~LinkedList() {
}

int LinkedList::Count() {
  return nNodes;
}

void LinkedList::AddFront(int value) {
  NodePtr newNode = new Node(value);

  if (!this->head) {
    this->head = newNode;
    this->tail = newNode;
  } else {
    newNode->next = this->head;
    this->head = newNode; 
  }
  this->nNodes++;
}

void LinkedList::Display(void) {
  if (!this->head) {
    return;
  }
  // There's at least one node in this list

  NodePtr cur = this->head;

  do {
    cout << setw(10) << cur->value << " ";
    cur = cur->next;  
  } while (cur != NULL);

  cout << endl;
}

void LinkedList::AddEnd(int value) {
  NodePtr newNode = new Node(value);

  if (!this->tail) {
    this->tail = newNode;
    this->head = newNode;
  } else {
    this->tail->next = newNode;
    this->tail = newNode;
  }
  this->nNodes++;
}

// Milestone 4: Add At Index Method
//
// This method takes two arguments: 
//
//  * an integer representing the value to add, and 
//  * another integer representing the index at which the node should be added. 
//
// Once you have an initial implementation, think about possible edge cases. 
// What possible index values are valid, and how will you deal with invalid indexes?

void LinkedList::AddAtIndex(int value, int index) {
  // exit function if:
  // * index is less than 0 or
  //   index is more than the number of nodes minus one
  //
  // * index is more than 0 and list is empty

  if (index < 0) {
    Debug::Log("AddAtIndex(): return early");
    return; 
  } else if (index >= 1 && nNodes == 0) {
    Debug::Log("AddAtIndex(): return early");
    return;
  } else if (index == 0) {
    Debug::Log("Add Element to the Front");
    AddFront(value);
  } else if (index == nNodes) {
    Debug::Log("Add Element to the End");
    AddEnd(value);
  } else if (index >= nNodes) {
    return;
  } else if (this->head) {
    Debug::Log("AddAtIndex()"); 
    int i = 0;
    NodePtr cur = this->head;

    // TODO:
    // Since we can't walk backwards
    // in a singly linked list,
    // we have to check for the node before 'index'.

    do {
     if (i == index - 1) {
       NodePtr newNode = new Node(value);
       // re-arrange
       NodePtr next = cur->next;
       cur->next = newNode;
       newNode->next = next;
       nNodes++;
       break;
     } 
     i++;
     cur = cur->next;
    } while (cur);
  } else {
    std::cout << "else case " << std::endl;
  }
  
  // if there are no elements, 
  // then return -1;
}

void LinkedList::RemoveFront(void) {
  if (this->head) {
    if (!this->head->next) {
      // ONLY ONE node;
      delete this->head;
      this->tail = NULL;
    } else {
      NodePtr front = this->head;
      NodePtr newFront = this->head->next;
      delete front;
      this->head = newFront;
    }
    this->nNodes--;
  }
  // else ZERO nodes
}

void LinkedList::RemoveEnd(void) {
  // because a Singly LinkedList is uni-directional
  // (head to tail),
  // walk from head to tail and record the Node before the end.

  if (this->tail) {
  }
}

void LinkedList::RemoveAtIndex(int index) {

  // return if no nodes in list 
  if (nNodes <= 0) {
    return;
  }

  if (index == 0 and nNodes == 1) {
    RemoveFront();
  } else if (index == nNodes - 1) {
    RemoveEnd();
  }
  // INCOMPLETE
}

int LinkedList::GetAtIndex(int index) {
  if (this->head) {
    int i = 0;
    NodePtr cur = this->head;
    int value = -1;

    do {
     if (i == index) {
       value = cur->value;
       return value;
     } 
     i++;
     cur = cur->next;
    } while (cur);
  }
  // if there are no elements, 
  // then return -1;

  return -1;
}

