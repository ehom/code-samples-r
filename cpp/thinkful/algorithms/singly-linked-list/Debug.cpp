//
//
//

#include <iostream>

#include "Debug.h"

void Debug::Log(const std::string &message, int value) {
  std::cout << "DEBUG: " << message << value << std::endl;
}

void Debug::Log(const std::string &message) {
  std::cout << "DEBUG: " << message << std::endl;
}

